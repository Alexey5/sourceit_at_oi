package le.u_solnka.tests.other;

import le.u_solnka.pages.AboutPage;
import le.u_solnka.pages.BasePage;
import le.u_solnka.utils.AbstractTest;
import le.u_solnka.utils.Browser;
import le.u_solnka.utils.tools.Verify;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by user on 11.11.14.
 *
 */
public class AboutPageTest extends AbstractTest{

    /**
     * Value that shall be displayed as default after navigating to the About page.
     */
    protected static final int DEFAULT_COUNTER_VALUE = 0;

    /**
     * Default counter increment on the About page.
     */
    protected static final int COUNTER_INCREMENT = 1;

    /**
     * Scenario:
     * 1. Go to About
     * 2. {Verify} Default value = 0;
     *
     * 3. Click inc 1 time
     * 4  {Verify} Default value + 1
     *
     * 5. click until MAX
     * {Verify} += 1 each time
     *
     * 6.Click Reset
     * 7. {Verify} counter = 0
     */
    @Test
    public void testCounter(){

        // 1. Go to About
        AboutPage page = new BasePage().tryToGetToAboutPage();

        // 2. {Verify} Default value = 0;
        Verify.Equals(DEFAULT_COUNTER_VALUE, page.getCounter(), "Unexpected default value.");

        int total = DEFAULT_COUNTER_VALUE;
        for (; total <= 12; total += COUNTER_INCREMENT) {
            //Click
            page.clickIncreaseAjax();

            new WebDriverWait(Browser.getDriver(), 10)
                    .until(ExpectedConditions.textToBePresentInElementLocated(AboutPage.BY_COUNTER, "" + total));

            //Verify +1
            Verify.Equals(total, page.getCounter(), "Unexpected value after inc.");
        }
    }
}
