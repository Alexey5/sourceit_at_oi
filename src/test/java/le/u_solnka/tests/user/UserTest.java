package le.u_solnka.tests.user;

import le.u_solnka.pages.CreateUserPage;
import le.u_solnka.pages.EditUserPage;
import le.u_solnka.pages.HomePage;
import le.u_solnka.pages.UsersPage;
import le.u_solnka.utils.AbstractAutoLoginTest;
import le.u_solnka.utils.data.DataHolder;
import le.u_solnka.utils.data.URLsHolder;
import le.u_solnka.utils.entities.TestUser;
import le.u_solnka.utils.helpers.UiHelper;
import le.u_solnka.utils.tools.Log;
import le.u_solnka.utils.tools.Util;
import le.u_solnka.utils.tools.Verify;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * A container for User entity related tests.<br/>
 *
 * @author L.E.
 * @since 2013-08-06
 */
public class UserTest extends AbstractAutoLoginTest{

    /**
     * Value that shall be displayed as default for the total number of users.
     */
    protected static final int DEFAULT_USERS_VALUE = 0;

    /**
     * The tests verifies whether it is possible to add a new user from Home page.
     * Note: only required fields are filled in.
     *
     * <p>
     * Requirements:
     * <ul>
     *     <li>2.1</li>
     *     <li>2.4.2</li>
     *     <li>2.4.3</li>
     * </ul>
     * </p>
     * <p>
     * Scenario:
     * <ul>
     *     <li>Clean up th DB(no users except un-countable 'admin');
     *     <li>Login as admin;
     *     <li>Click "Add new user" on Home page;
     *     <li>Fill in the form(only required fields) and submit the form;
     *     <li>{Verify} Total users count has been increased (+1);
     *     <li>{Verify} There is corresponding record in the Users table;
     *     <li>Click to view the record;
     *     <li>{Verify} the form shows saved new user's data.
     * </ul>
     * </p>
     * ("enabled = false" below means that this test won't be executed even if you explicitly point to it since the test is ignored.)
     */
    @Test(enabled = true, groups={"user"}, description="Add new user from Home page.")
    public void testAddNewUserFromHomePage_OnlyRequiredFields(){
        //DB clean-up and logging as an admin is made in base class(es) in @BeforeMethod method(s).

        int expectedUserCount = DEFAULT_USERS_VALUE;

        Log.logStep("Click 'Add new user' on Home page;");
        //HomePage homePage = PageFactory.initElements(Browser.getDriver(), HomePage.class);

        //Assuming pre-condition methods worked well, we are on the "Home" page at the moment.
        //However, let's check one more time.
        Verify.isCurrentUrlContainsExpected(URLsHolder.getHolder().getPageHome(), "Current page is not 'Home'");

        Assert.assertEquals(new HomePage().getLoggedUserName(), DataHolder.getHolder().getAdminName(),
                "Current user is not " + DataHolder.getHolder().getAdminName());

        CreateUserPage createUserPage = new HomePage().gotoCreateNewUser();

        //Create and fill in "user-to-create" object.
        TestUser userToCreate = new TestUser(
                Util.getSomeVeryShortValue(),
                Util.getSomeVeryShortValue(),
                Util.getSomeShortValue(),
                Util.getSomeValue(),
                "2014-11-02");

        Log.logInfo("User-to-create: " + userToCreate);

        Log.logStep("Fill in the form(only required fields) and submit the form;");
        createUserPage.fillInTheForm(userToCreate);

        createUserPage.submit();
        expectedUserCount += 1;

        UsersPage usersPage = new UsersPage();

        //{Verify} Total users count has been increased (+1);
        Verify.Equals(expectedUserCount, usersPage.getNumberOfUsers(), "Unexpected number of users after adding new one");

        //{Verify} There is corresponding record in the Users table;
        TestUser createdUser = new TestUser(
                usersPage.getUserLoginNameFromTable(expectedUserCount),
                usersPage.getUserFirstNameFromTable(expectedUserCount),
                usersPage.getUserLastNameFromTable(expectedUserCount),
                usersPage.getUserPasswordFromTable(expectedUserCount),
                Util.convertToTableDate(usersPage.getUserBirthDateFromTable(expectedUserCount)));

        Log.logInfo("User is displayed in the table: " + createdUser);

        Verify.Equals(userToCreate, createdUser, "Invalid user record in the table");

        //Click to view the record;
        usersPage.clickUserLink(expectedUserCount);

        EditUserPage editUserPage = new EditUserPage();

        //{Verify} the form shows saved new user's data
        TestUser createdUserOnEdit = new TestUser(
                editUserPage.getLogin(),
                editUserPage.getFirstName(),
                editUserPage.getLastName(),
                editUserPage.getPassword(),
                editUserPage.getBirthday());

        Log.logInfo("User is displayed on the edit page: " + createdUserOnEdit);

        Verify.Equals(userToCreate, createdUserOnEdit, "Invalid user record on the Edit User page");
    }
}