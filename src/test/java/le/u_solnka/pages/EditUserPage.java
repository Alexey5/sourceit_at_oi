package le.u_solnka.pages;

import le.u_solnka.utils.entities.TestUser;
import le.u_solnka.utils.helpers.UiHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;

import java.util.List;

/**
 * PageObject class for "EditUser" page.
 */
public class EditUserPage extends BasePage{
    /**
     * ById locator for Honorific selector.
     */
    private static final By BY_SLCT_HONORIFIC = new By.ById("honorific");

    /**
     * ById locator for loginName field.
     */
    private static final By BY_TB_LOGINNAME = new By.ById("loginName");

    /**
     * ById locator for firstUserName field.
     */
    private static final By BY_TB_FIRSTNAME = new By.ById("firstUserName");

    /**
     * ById locator for middleUserName field.
     */
    private static final By BY_TB_MIDDLENAME = new By.ById("middleUserName");

    /**
     * ById locator for lastUserName field.
     */
    private static final By BY_TB_LASTNAME = new By.ById("lastUserName");

    /**
     * ById locator for age field.
     */
    private static final By BY_TB_AGE = new By.ById("age");

    /**
     * ById locator for password field.
     */
    private static final By BY_TB_PASSWORD = new By.ById("psswd");

    /**
     * ById locator for birth date field.
     */
    private static final By BY_TB_BIRTHDATE = new By.ById("dateId");

    /**
     * ById locator for isActive? field.
     */
    private static final By BY_TB_ACTIVE = new By.ById("isActiveId");

    public EditUserPage() {
    }

    /**
     * Gets LoginName value from the EditUserPage.
     *
     * @return LoginName value from from the EditUserPage.
     */
    public String getLogin(){
        return UiHelper.getAttributeValue(BY_TB_LOGINNAME, "value");
    }

    /**
     * Gets FirstName value from the EditUserPage.
     *
     * @return FirstName value from from the EditUserPage.
     */
    public String getFirstName(){
        return UiHelper.getAttributeValue(BY_TB_FIRSTNAME, "value");
    }

    /**
     * Gets LastName value from the EditUserPage.
     *
     * @return LastName value from from the EditUserPage.
     */
    public String getLastName(){
        return UiHelper.getAttributeValue(BY_TB_LASTNAME, "value");
    }

    /**
     * Gets Password value from the EditUserPage.
     *
     * @return Password value from from the EditUserPage.
     */
    public String getPassword(){
        return UiHelper.getAttributeValue(BY_TB_PASSWORD, "value");
    }

    /**
     * Gets Birthday value from the EditUserPage.
     *
     * @return Birthday value from from the EditUserPage.
     */
    public String getBirthday(){
        return UiHelper.getAttributeValue(BY_TB_BIRTHDATE, "value");
    }

    /**
     * Gets isActive value from the EditUserPage.
     *
     * @return isActive value from from the EditUserPage.
     */
    public boolean getIsActive(){
        WebElement wb = driver.findElement(BY_TB_ACTIVE);
        return wb.isSelected();
    }
}
