package le.u_solnka.pages;

import le.u_solnka.utils.helpers.UiHelper;
import org.openqa.selenium.By;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;

/**
 * PageObject class for "Users" page.
 */
public class UsersPage extends BasePage {
    private static final Logger logger = LoggerFactory.getLogger(UsersPage.class);

    /**
     * ById locator for Total Records field.
     */
    private static final By BY_TB_TOTAL = new By.ById("tbTotal");

    /**
     * ById locator for "New User" link.
     */
    private static final By BY_LNK_NEW_USER = new By.ById("newUser");

    /**
     * A placeholder string to be replaced by something.
     */
    private static final String PLACEHOLDER = "${replaceWithNumber}";

    /**
     * Base CSS locator pattern for the table.
     */
    private static final String CSS_BASE_TABLE_PATTERN = String.format("table#usrs>tbody>tr:nth-of-type(%s)>td", PLACEHOLDER);

    /**
     * Locator pattern for cells in LoginName column.
     */
    private static final String CSS_TABLE_LOGIN_PATTERN = CSS_BASE_TABLE_PATTERN + ".loginName>a";

    /**
     * Locator pattern for cells in FirstName column.
     */
    private static final String CSS_TABLE_FIRSTNAME_PATTERN = CSS_BASE_TABLE_PATTERN + ".firstUserName";

    /**
     * Locator pattern for cells in LastName column.
     */
    private static final String CSS_TABLE_LASTNAME_PATTERN = CSS_BASE_TABLE_PATTERN + ".lastUserName";

    /**
     * Locator pattern for cells in BirthDate column.
     */
    private static final String CSS_TABLE_BIRTHDATE_PATTERN = CSS_BASE_TABLE_PATTERN + ".birthDate";

    /**
     * Locator pattern for cells in Password column.
     */
    private static final String CSS_TABLE_PASSWORD_PATTERN = CSS_BASE_TABLE_PATTERN + ".psswd";

    public UsersPage() {
    }

    /**
     * Clicks "Add new user" link under Actions.
     *
     * @return - CreateUserPage PageObject.
     */
    public CreateUserPage addNewUser() {
        logger.debug("Opening CreateNewUser page from Users page.");

        UiHelper.click(BY_LNK_NEW_USER);

        return new CreateUserPage();
    }

    /**
     * Gets total number of users from BY_TB_TOTAL field.
     *
     * @return - CreateUserPage PageObject.
     */
    public int getNumberOfUsers(){
        try {
            return Integer.parseInt(UiHelper.getAttributeValue(BY_TB_TOTAL, "value"));
        } catch (NumberFormatException e) {
            logger.error(String.format("Cannot parse string(%s) as int"),
                    UiHelper.getAttributeValue(BY_TB_TOTAL, "value"));
        }
        return 0;
    }

    /**
     * Gets LoginName value from the cell in {tableRowNumber} row.
     *
     * @param tableRowNumber - table row number(starting from 1).
     *
     * @return LoginName value from the cell in {tableRowNumber} row.
     */
    public String getUserLoginNameFromTable(int tableRowNumber){
        return getValueFromTableCell(CSS_TABLE_LOGIN_PATTERN, tableRowNumber);
    }

    /**
     * Gets FirstName value from the cell in {tableRowNumber} row.
     *
     * @param tableRowNumber - table row number(starting from 1).
     *
     * @return FirstName value from the cell in {tableRowNumber} row.
     */
    public String getUserFirstNameFromTable(int tableRowNumber){
        return getValueFromTableCell(CSS_TABLE_FIRSTNAME_PATTERN, tableRowNumber);
    }

    /**
     * Gets LastName value from the cell in {tableRowNumber} row.
     *
     * @param tableRowNumber - table row number(starting from 1).
     *
     * @return LastName value from the cell in {tableRowNumber} row.
     */
    public String getUserLastNameFromTable(int tableRowNumber){
        return getValueFromTableCell(CSS_TABLE_LASTNAME_PATTERN, tableRowNumber);
    }

    /**
     * Gets Password value from the cell in {tableRowNumber} row.
     *
     * @param tableRowNumber - table row number(starting from 1).
     *
     * @return Password value from the cell in {tableRowNumber} row.
     */
    public String getUserPasswordFromTable(int tableRowNumber){
        return getValueFromTableCell(CSS_TABLE_PASSWORD_PATTERN, tableRowNumber);
    }

    /**
     * Gets BirthDate value from the cell in {tableRowNumber} row.
     *
     * @param tableRowNumber - table row number(starting from 1).
     *
     * @return BirthDate value(as String) from the cell in {tableRowNumber} row.
     */
    public String getUserBirthDateFromTable(int tableRowNumber){
        return getValueFromTableCell(CSS_TABLE_BIRTHDATE_PATTERN, tableRowNumber);
    }

    private String getValueFromTableCell(String columnCssLocatorPattern, int tableRowNumber){
        String css = columnCssLocatorPattern.replace(PLACEHOLDER, Integer.toString(tableRowNumber));

        return UiHelper.getText(By.cssSelector(css));
    }

    /**
     * Clicks on the loginName link in {tableRowNumber} row.
     *
     * @param tableRowNumber - table row number(starting from 1).
     *
     * @return void.
     */
    public void clickUserLink(int tableRowNumber){
        String temp = CSS_TABLE_LOGIN_PATTERN;
        UiHelper.click(By.cssSelector(temp.replace(PLACEHOLDER, Integer.toString(tableRowNumber))));
    }
}