package le.u_solnka.pages;

import le.u_solnka.utils.helpers.UiHelper;
import org.openqa.selenium.By;

/**
 * Created by user on 12.11.14.
 */
public class AboutPage extends BasePage{

    /**
     * By object for Counter link.
     */

    public static final By BY_COUNTER = new By.ByXPath("//*[starts-with(@id, 'zone')]/p/strong");

    public int getCounter(){
        return Integer.parseInt(UiHelper.getText(BY_COUNTER));
    }

    public void clickIncreaseAjax(){
        UiHelper.click(By.id("incrementAjax"));
    }
}
